//
//  GoogleMapViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/17/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SVProgressHUD

protocol GoogleMapViewDelegate: class {
    func showPropertyList(index: Int)
}

class GoogleMapViewController: UIViewController {
    
    weak var mDelegate: GoogleMapViewDelegate?
    var latArray: Array<CLLocationDegrees> = [28.630613,28.629141,28.630461,28.629158,28.633489]
    var lngArray: Array<CLLocationDegrees> = [77.378509,77.379145,77.382615,77.380282,77.380572]
    var mapView = GMSMapView()
    var sourceLatAndLng:CLLocationCoordinate2D = CLLocationCoordinate2DMake(28.630613, 77.378509);
    var destLatAndLng:CLLocationCoordinate2D = CLLocationCoordinate2DMake(28.588938, 77.302186);
    @IBOutlet weak var mapViewButton: UIButton!
    @IBOutlet weak var mapViewCustom: UIView!
    //Propert List = 0, Property Map = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.loadMap()
            SVProgressHUD.dismiss()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
         mapViewButton.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 5.0)
    }
    
    @IBAction func btnPropertyMapAction(_ sender: Any) {
            mDelegate?.showPropertyList(index: 0)
    }
    
    func loadMap(){
        
        let camera = GMSCameraPosition.camera(withLatitude: 28.631459, longitude: 77.381863, zoom: 16.0)
            self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        
//        do {
//          // Set the map style by passing the URL of the local file.
//          if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
//            mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
//          } else {
//            NSLog("Unable to find style.json")
//          }
//        } catch {
//          NSLog("One or more of the map styles failed to load. \(error)")
//        }
        
            self.view.addSubview(self.mapView)

               // Creates a marker in the center of the map.
               let marker = GMSMarker()
               marker.position = CLLocationCoordinate2D(latitude: 28.631459, longitude: 77.381863)
               marker.title = "Wildnet"
               marker.snippet = "Technologies"
            marker.map = self.mapView
            self.mapView.frame = self.view.frame
            self.mapViewCustom.addSubview(self.mapView)
        
        
            for i in 0 ..< self.latArray.count {
            let state_marker = GMSMarker()
                state_marker.position = CLLocationCoordinate2D(latitude: self.latArray[i], longitude: self.lngArray[i])
            state_marker.title = "Title"
            state_marker.snippet = "Sub Title"
            state_marker.icon = UIImage.init(named: "pinRed")
                state_marker.map = self.mapView
        }
            //self.getPolylineRoute(from: self.sourceLatAndLng, to: self.destLatAndLng)
    }

}


extension GoogleMapViewController{
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving&key=AIzaSyBAxa6a0c9JBihWbhqItgqWIhNsvoiD47o")!

        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                //self.activityIndicator.stopAnimating()
            }
            else {
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{

                        guard let routes = json["routes"] as? NSArray else {
                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()
                            }
                            return
                        }

                        if (routes.count > 0) {
                            let overview_polyline = routes[0] as? NSDictionary
                            let dictPolyline = overview_polyline?["overview_polyline"] as? NSDictionary

                            let points = dictPolyline?.object(forKey: "points") as? String

                            self.showPath(polyStr: points!)

                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()

                               let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
                               let update = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 170, left: 30, bottom: 30, right: 30))
                                self.mapView.moveCamera(update)
                            }
                        }
                        else {
                            DispatchQueue.main.async {
                                //self.activityIndicator.stopAnimating()
                            }
                        }
                    }
                }
                catch {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                        //self.activityIndicator.stopAnimating()
                    }
                }
            }
        })
        task.resume()
    }

    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.strokeColor = UIColor.red
        polyline.map = mapView // Your map view
    }
}
