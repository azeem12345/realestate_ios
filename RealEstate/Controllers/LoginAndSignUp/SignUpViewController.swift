//
//  SignUpViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/6/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SignUpViewController: BaseClassVC {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var imgConfirmPassword: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNeedsStatusBarAppearanceUpdate()
        btnSignIn.underline()
        tfPassword.isSecureTextEntry = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        if(tfEmail.text == ""){
             Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterEmail)
             return
           }else if(!String.isEmailValid(email: tfEmail.text?.trimmingCharacters(in: .whitespaces))){
             Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterValidEmail)
             return
           }else if(tfPassword.text?.trimmingCharacters(in: .whitespaces) == ""){
             Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterPassword)
              return
           }else if(tfConfirmPassword.text?.trimmingCharacters(in: .whitespaces) == ""){
             Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterConfirmPassword)
           return
        }else if(tfConfirmPassword.text?.trimmingCharacters(in: .whitespaces) != tfPassword.text?.trimmingCharacters(in: .whitespaces)){
             Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterSameConfirmPassword)
           return
        }
                
    }
    
    @IBAction func btnSignInAction(_ sender: AnyObject) {
        goingToLoginScreen()
    }
    
    func goingToLoginScreen() {
        do {
            var vc: UIViewController?
            var found: Bool = false
            if let viewC = (self.navigationController?.viewControllers){
                for view: UIViewController in viewC {
                    if (view is LoginViewController) {
                        vc = (view as? LoginViewController)
                        found = true
                        break
                    }
                }
            }
            if found {
                self.navigationController?.popToViewController(vc!, animated: true)
            }else{
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    
    @IBAction func btnPasswordAction(_ sender: AnyObject) {
        if(tfPassword.isSecureTextEntry) {
            imgPassword.image = UIImage.init(named: "viewPassword")
            tfPassword.isSecureTextEntry = false
        } else {
            imgPassword.image = UIImage.init(named: "hide")
            tfPassword.isSecureTextEntry = true
        }
    }

    @IBAction func btnConfirmPasswordAction(_ sender: AnyObject) {
        if(tfConfirmPassword.isSecureTextEntry) {
            imgConfirmPassword.image = UIImage.init(named: "viewPassword")
            tfConfirmPassword.isSecureTextEntry = false
        } else {
            imgConfirmPassword.image = UIImage.init(named: "hide")
            tfConfirmPassword.isSecureTextEntry = true
        }
    }

    
}
