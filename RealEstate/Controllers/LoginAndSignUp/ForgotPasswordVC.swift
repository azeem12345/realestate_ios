//
//  ForgotPasswordVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/13/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class ForgotPasswordVC: BaseClassVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordSuccessPopup") as! ForgotPasswordSuccessPopup
        controller.mDelegate = self
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        controller.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        self.present(controller, animated: true, completion: nil)
    }

}

extension ForgotPasswordVC: ForgotPasswordSuccessPopupDelegate{
    func redirectionToSignin() {
        self.navigationController?.popViewController(animated: false)
    }
}
