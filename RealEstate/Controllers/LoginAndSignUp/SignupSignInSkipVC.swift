//
//  SignupSignInSkipVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/6/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SignupSignInSkipVC: UIViewController {
    
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSkip: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNeedsStatusBarAppearanceUpdate()
        btnSignIn.layer.borderColor = UIColor.white.cgColor
        btnSignIn.layer.borderWidth = 1
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    

    @IBAction func btnSignUpAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func btnSignInAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func btnSkipAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
