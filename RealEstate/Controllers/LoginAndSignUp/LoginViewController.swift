//
//  LoginViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/27/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class LoginViewController: BaseClassVC {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNeedsStatusBarAppearanceUpdate()
        btnSignUp.underline()
        
//        let loginButton = FBLoginButton()
//        loginButton.center = view.center
//        loginButton.permissions = ["public_profile", "email"]
//        view.addSubview(loginButton)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnSignInAction(_ sender: Any) {
//        if(tfEmail.text == ""){
//            Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterEmail)
//            return
//        }else if(!String.isEmailValid(email: tfEmail.text?.trimmingCharacters(in: .whitespaces))){
//            Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterValidEmail)
//            return
//        }
//        else if(tfPassword.text == ""){
//            Utility.showAlertAtTopOfWindowWithMessage(message: Constants.pleaseEnterPassword)
//            return
//        }
        
        goingToHomeScreen()
        
    }
    
    func goingToHomeScreen(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        //SavedPhotosVC
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnFacebookAction(_ sender: Any) {
        getFacebookUserInfo()
    }
    
    @IBAction func btnGoogleLoginAction(_ sender: Any) {
       loginByGoogle()
    }
    
    @IBAction func btnSignUpAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension LoginViewController{
    func getFacebookUserInfo(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email ], viewController: self) { (result) in
            switch result{
            case .cancelled:
                print("Cancel button click")
            case .success:
                let params = ["fields" : "id, name, first_name, last_name, picture.type(large), email "]
                let graphRequest = GraphRequest.init(graphPath: "/me", parameters: params)
                let Connection = GraphRequestConnection()
                Connection.add(graphRequest) { (Connection, result, error) in
                    let info = result as! Dictionary<String, Any>
                    
                    if let imageURL = ((info["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                        print(imageURL)
                    }
                    
                    print(info["name"] as! String)
                    self.goingToHomeScreen()
                }
                Connection.start()
                
//                AccessToken.current = nil
//                Profile.current = nil
//                loginManager.logOut()
                
            default:
                print("??")
            }
        }
    }
    
    
}


extension LoginViewController: GIDSignInDelegate{
    func loginByGoogle() {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    //MARK:Google SignIn Delegate
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        // myActivityIndicator.stopAnimating()
    }
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }

    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          print("The user has not signed in before or they have since signed out.")
        } else {
          print("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
      let userId = user.userID                  // For client-side use only!
      let idToken = user.authentication.idToken // Safe to send to the server
      let fullName = user.profile.name
      let givenName = user.profile.givenName
      let familyName = user.profile.familyName
      let email = user.profile.email
        let dimension = round(100 * UIScreen.main.scale)
        let pic = user.profile.imageURL(withDimension: UInt(dimension))
        print("userId\(String(describing: userId)), idToken\(String(describing: idToken)), fullName\(String(describing: fullName)), givenName\(String(describing: givenName)), familyName\(String(describing: familyName)), email\(String(describing: email)), pic\(String(describing: pic))")
      // ...
        self.goingToHomeScreen()
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
    }
    
    
}
