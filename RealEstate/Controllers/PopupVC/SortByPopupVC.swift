//
//  SortByPopupVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/5/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SortByPopupVC: BaseClassVC {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnReset: UIButton!
    var sortByArray = Array<String>()
    var selectedIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        sortByArray = ["Date: New to Old","Price - High to Low","Price - Low to High","Area/sq. ft. - High to Low","Area/sq. ft. - Low to High"]
        btnReset.layer.borderWidth = 1
        btnReset.layer.borderColor = UIColor.redColor().cgColor
    }

}

extension SortByPopupVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! SortByTableViewCell
    
        if(selectedIndex == indexPath.row){
            cell.lblTitle.textColor = UIColor.grayColor95()
            cell.lblTitle.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        }else{
            cell.lblTitle.textColor = UIColor.grayColor115()
            cell.lblTitle.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        }
        
        cell.lblTitle.text = sortByArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
