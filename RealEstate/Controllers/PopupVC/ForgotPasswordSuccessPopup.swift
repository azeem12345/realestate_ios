//
//  ForgotPasswordSuccessPopup.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/13/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

protocol ForgotPasswordSuccessPopupDelegate: class {
    func redirectionToSignin()
}

class ForgotPasswordSuccessPopup: BaseClassVC {

    weak var mDelegate: ForgotPasswordSuccessPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSignInAction(_ sender: Any) {
        dismissView()
        mDelegate?.redirectionToSignin()
    }

    func goingToSignUpScreen() {
        do {
            var vc: UIViewController?
            var found: Bool = false
            if let viewC = (self.navigationController?.viewControllers){
                for view: UIViewController in viewC {
                    if (view is LoginViewController) {
                        vc = (view as? LoginViewController)
                        found = true
                        break
                    }
                }
            }
            if found {
                self.navigationController?.popToViewController(vc!, animated: false)
            }else{
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
}
