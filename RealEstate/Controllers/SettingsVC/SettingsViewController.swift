//
//  SettingsViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/14/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SettingsViewController: BaseClassVC {
    
    @IBOutlet weak var headerView: UIView!
    var titleArray1 = Array<Any>(), titleArray2 = Array<Any>()

    override func viewDidLoad() {
        super.viewDidLoad()
        titleArray1 = ["Country","Currency","Language"]
        titleArray2 = ["India","INR","English"]
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerView.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
        //print(countries)
    }
    
    var countries: [String] = {

        var arrayOfCountries: [String] = []

        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            arrayOfCountries.append(name)
        }

        return arrayOfCountries
    }()


}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray1.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! SettingsTableViewCell
        cell.lblTitle.text = titleArray1[indexPath.row] as? String
        cell.lblTitle2.text = titleArray2[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
