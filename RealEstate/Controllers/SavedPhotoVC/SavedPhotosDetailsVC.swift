//
//  SavedPhotosDetailsVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/28/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SavedPhotosDetailsVC: BaseClassVC {

    @IBOutlet weak var collectionView: UICollectionView!
    var arraySelectedPhotos = Set<Int>()
    @IBOutlet weak var bottomView: BottomViewForPhotos!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    }
    
    func setData(){
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .left,
                                                                        verticalAlignment: .center)
        collectionView.collectionViewLayout = alignedFlowLayout
        let view = BottomViewForPhotos.instanceFromNib()
        view.exportButton.addTarget(self, action: #selector(exportButtonAction), for: .touchUpInside)
        view.sendButton.addTarget(self, action: #selector(sendButtonAction), for: .touchUpInside)
        view.sparklesButton.addTarget(self, action: #selector(sparklesButtonAction), for: .touchUpInside)
        view.deleteButton.addTarget(self, action: #selector(deleteButtonAction), for: .touchUpInside)
        self.bottomView.addSubview(view)
    }

}

extension SavedPhotosDetailsVC{
    @objc func exportButtonAction(sender: UIButton!) {
      print("exportButtonAction")
    }
    @objc func sendButtonAction(sender: UIButton!) {
      print("sendButtonAction")
    }
    @objc func sparklesButtonAction(sender: UIButton!) {
      print("sparklesButtonAction")
    }
    @objc func deleteButtonAction(sender: UIButton!) {
      print("deleteButtonAction")
    }
}

extension SavedPhotosDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "Cell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SavedPhotoDetailCell
        cell.selectedView.isHidden = true
        if arraySelectedPhotos.contains(indexPath.row){
            cell.selectedView.isHidden = false
            cell.isSelected = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.arraySelectedPhotos.contains(indexPath.row)){
            self.arraySelectedPhotos.remove(indexPath.row)
        }else{
            self.arraySelectedPhotos.insert(indexPath.row)
        }
        self.collectionView.reloadItems(at: [indexPath])
        //self.collectionView.reloadData()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.collectionView.frame.width/3) - 10
        return CGSize(width: size+3, height: size)
    }

}
