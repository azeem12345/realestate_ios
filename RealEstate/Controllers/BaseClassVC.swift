//
//  BaseClassVC.swift
//  Beno
//
//  Created by Algoworks on 28/01/19.
//  Copyright © 2019 Algoworks. All rights reserved.
//

import UIKit

class BaseClassVC: UIViewController ,UITextFieldDelegate{
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadInitialUI()
        self.loadInitialData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unRegisterForKeyboardNotifications()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadInitialUI(){
    }
    func loadInitialData(){
    }
    
    func registerForKeyboardNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func unRegisterForKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        let info: [AnyHashable: Any]? = notification.userInfo
        var keyboardRect = info?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? CGRect.zero
        keyboardRect = view.convert(keyboardRect, from: nil)
        if(scrollView != nil){
            var contentInset: UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardRect.size.height + 20
            scrollView.contentInset = contentInset
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if(scrollView != nil){
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollView?.contentInset = contentInset
        }
    }
    
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            return textField.resignFirstResponder()
        }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissView(){
        self.view.endEditing(true)
        self.dismiss(animated: true) {}
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        dismissView()
    }
    
//    @IBAction func btnOpenLeftMenuAction(_ sender: Any) {
//        slideMenuController()?.openLeft()
//    }
    
}
