//
//  AddCardViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/8/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class AddCardViewController: BaseClassVC, UIGestureRecognizerDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var cardNumberView: UIView!
    @IBOutlet weak var expireDateView: UIView!
    @IBOutlet weak var secureCodeView: UIView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var btnSaveThisCard: UIButton!
    @IBOutlet weak var tfCardNumber: UITextField!
    @IBOutlet weak var tfExpiryDate: UITextField!
    @IBOutlet weak var tfCVVNumber: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfCommon: UITextField!
    @IBOutlet weak var expiryDatePickerView: MonthYearPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tfCommon = tfCardNumber
        expiryDatePickerView.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: -2), radius: 1, scale: true)
        headerView.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
        cardNumberView.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 5.0)
        expireDateView.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 5.0)
        secureCodeView.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 5.0)
        nameView.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 5.0)
        
        expiryDatePickerView.isHidden = true
        expiryDatePickerView.onDateSelected = { (month: Int, year: Int) in
            let string = String(format: "%02d/%d", month, year)
            self.tfExpiryDate.text = string
        }
        
        let tapG = UITapGestureRecognizer.init(target: self, action: #selector(hideView(_:)))
        tapG.delegate = self
        self.view.addGestureRecognizer(tapG)
        self.view.tag = 100
        //let cardNumber = tfCardNumber.text?.replacingOccurrences(of: "-", with: "")
    }
    
    @objc func hideView(_ sender: UITapGestureRecognizer){
        self.expiryDatePickerView.isHidden = true
        self.tfCommon.resignFirstResponder()
    }
    
    @IBAction func btnShowExpiryDatePickerViewAction(_ sender: Any){
        tfCommon.resignFirstResponder()
        expiryDatePickerView.isHidden = false
    }
    
    @IBAction func btnSaveThisCardAction(_ sender: Any) {
        btnSaveThisCard.isSelected = !btnSaveThisCard.isSelected
    }

}


extension AddCardViewController{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        tfCommon = textField
        expiryDatePickerView.isHidden = true
        if textField == tfExpiryDate{
            expiryDatePickerView.isHidden = false
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if textField == tfCardNumber{
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            let textFieldValue = textField.text?.replacingOccurrences(of: "-", with: "")
            if((textFieldValue?.count)! % 4 == 0 && isBackSpace != -92 && (textField.text?.count)! != 0 && (textField.text?.count)! <= 16){
                textField.text = (textField.text! + "-").replacingOccurrences(of: "--", with: "-")
            }
            let maxLength = 19
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            //Utils.getToolbar(textField: textField)
            return newString.length <= maxLength && string == numberFiltered
        }else
        if textField == tfExpiryDate{
            return false
        }else
        if textField == tfCVVNumber{
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            
            let maxLength = 3
            let currentString: NSString = tfCVVNumber.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength && string == numberFiltered
        }
        
        return true
    }
}
