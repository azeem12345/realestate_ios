//
//  PaymentViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/8/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class PaymentViewController: BaseClassVC {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet var collectionView: UICollectionView!
    
    var centerFlowLayout: SJCenterFlowLayout {
        return collectionView.collectionViewLayout as! SJCenterFlowLayout
    }
    var scrollToEdgeEnabled: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.register(UINib(nibName: "PaymentCardCVCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        
        let layout = SJCenterFlowLayout()
        layout.itemSize = CGSize(width: view.bounds.width * 0.85, height: 200)
        collectionView.collectionViewLayout = layout
               
        centerFlowLayout.animationMode = SJCenterFlowLayoutAnimation.scale(sideItemScale: 0.7, sideItemAlpha: 0.2, sideItemShift: 0.0)
        centerFlowLayout.scrollDirection = .horizontal
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bottomView.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: -2), radius: 1, scale: true)
        headerView.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
    }
    
    @IBAction func btnAddCardAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }


}

extension PaymentViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "Cell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.scrollToEdgeEnabled, let cIndexPath = centerFlowLayout.currentCenteredIndexPath,
            cIndexPath != indexPath {
            centerFlowLayout.scrollToPage(atIndex: indexPath.row)
        }
    }
    
}
