//
//  LocationConfirmationVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/11/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SVProgressHUD

class LocationConfirmationVC: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var customMapView: UIView!
    var mapView = GMSMapView()
    @IBOutlet weak var mainViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var addressSearchViewHeightConst: NSLayoutConstraint!
    var searchVC: AddressSearchViewController!
    @IBOutlet weak var viewForSearchVC: UIView!
    @IBOutlet weak var tfSearch: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewForSearchVC.isHidden = true
        tfSearch?.addTarget(self, action: #selector(textFieldDidChange(textField:)),for: .editingChanged)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        mainView.roundCorners(corners: [.topLeft, .topRight], radius: 30.0)
        LocationClass.shared.mDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.unRegisterForKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadMap()
    }

    func loadMap(){
        
        let condinate = LocationClass.shared.locationManager.location?.coordinate
                
        let camera = GMSCameraPosition.camera(withLatitude: condinate?.latitude ?? 0.0, longitude: condinate?.longitude  ?? 0.0, zoom: 14.0)
                    self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)

                       let marker = GMSMarker()
                       marker.position = CLLocationCoordinate2D(latitude: condinate?.latitude ?? 0.0, longitude: condinate?.longitude  ?? 0.0)
                       marker.title = "Wildnet"
                       marker.snippet = "Technologies"
                       marker.icon = UIImage.init(named: "currentLocation")
                
                    marker.map = self.mapView
                    //self.mapView.delegate = self
                    self.mapView.frame = CGRect(x: 0, y: 0, width: self.customMapView.frame.width, height: self.customMapView.frame.height)
                    //self.mapView.isMyLocationEnabled = true
                    self.customMapView.addSubview(self.mapView)
                   
            }
    
    func removeSubView(){
        if let viewWithTag = self.view.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    func addSearchViewController(textField: UITextField){
        removeSubView()
        searchVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressSearchViewController") as? AddressSearchViewController
        searchVC.txtField = textField
        let condinate = LocationClass.shared.locationManager.location?.coordinate
        searchVC.coordinateCurrent = condinate
        searchVC.mDelegate = self
        searchVC.view.frame = CGRect(x: 0, y: 0, width: self.viewForSearchVC.frame.width, height: 200)
        addChild(searchVC)
        searchVC.view.tag = 100
        self.viewForSearchVC.addSubview(searchVC.view)
    }
    

}

extension LocationConfirmationVC: LocationClassDelegate, UITextFieldDelegate, AddressSearchViewDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        SVProgressHUD.dismiss()
        addSearchViewController(textField: textField)
        return true
    }
    func getCurrentLocation() {
        loadMap()
        print("Call method::\(String(describing: LocationClass.shared.locationManager.location?.coordinate.latitude))")
    }
    func hideAndShowSearchView(countData: Int) {
            self.viewForSearchVC.isHidden = true
        if(countData > 0){
            self.viewForSearchVC.isHidden = false
        }
    }
    @objc func textFieldDidChange(textField: UITextField) {
        if(textField.text == ""){
            self.viewForSearchVC.isHidden = true
        }
    }
}

extension LocationConfirmationVC{
    func registerForKeyboardNotifications(){
           NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
           
           NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
       func unRegisterForKeyboardNotifications(){
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
       }
       
       @objc func keyboardWillShow(_ notification: Notification) {
            mainViewHeightConst.constant = self.view.frame.height - 40
        if(self.view.frame.height > 667){
            addressSearchViewHeightConst.constant = self.view.frame.height - 600
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.mapView.frame = CGRect(x: 0, y: 0, width: self.customMapView.frame.width, height: self.view.frame.height - 460)
        }
       }
       
       @objc func keyboardWillHide(_ notification: Notification) {
        self.viewForSearchVC.isHidden = true
            mainViewHeightConst.constant = 475
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.mapView.frame = CGRect(x: 0, y: 0, width: self.customMapView.frame.width, height: self.customMapView.frame.height)
        }
       }
       
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
               return textField.resignFirstResponder()
        }
    
    @objc func dismissView(){
        self.view.endEditing(true)
        self.dismiss(animated: true) {}
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        dismissView()
    }
}
