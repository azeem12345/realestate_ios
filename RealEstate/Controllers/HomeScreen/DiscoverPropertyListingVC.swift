//
//  DiscoverPropertyListingVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/29/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

protocol DiscoverPropertyListingDelegate: class {
    func showMapPropertyListing(index: Int)
}

class DiscoverPropertyListingVC: UIViewController {

    weak var mDelegate: DiscoverPropertyListingDelegate?
    @IBOutlet weak var tableView: UITableView!
    var gradientView: UIView = UIView()
    @IBOutlet weak var viewTopBar: UIView!
     @IBOutlet weak var viewForSearchBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    func loadData(){
        tableView.register(UINib(nibName: "PropertyListTableViewCell", bundle: nil), forCellReuseIdentifier: "PropertyListTableViewCell")
        viewTopBar.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
        viewForSearchBar.layer.borderColor = UIColor.grayColor().cgColor
        viewForSearchBar.layer.borderWidth = 1

    }
    
    
    override func viewDidAppear(_ animated: Bool) {

       }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        gradientView.frame = CGRect(x: 0, y: 500, width: self.view.frame.width, height: self.view.frame.height - 500)
        gradientView.backgroundColor = .clear
        gradientView.isUserInteractionEnabled = false
        gradientView.addGradientWithColor(color: .white)
        self.view.addSubview(gradientView)
    }
    
    @IBAction func btnFiltersAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnSortByAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SortByPopupVC") as! SortByPopupVC
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        controller.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        self.present(controller, animated: true, completion: nil)
    }
    

}

extension DiscoverPropertyListingVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "PropertyListTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! PropertyListTableViewCell
        cell.layoutIfNeeded()
        cell.carousel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width-40, height: cell.carouselView.frame.height)
        cell.carouselView.addSubview(cell.carousel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 477
    }
}
