//
//  HomeViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/29/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class HomeViewController: BaseClassVC {
    
    @IBOutlet weak var container: PageViewController!
    var slectedCellIndex = 0
    @IBOutlet weak var collectionView: UICollectionView!
    var imgUnSelectedArray = Array<Any>(), imgSelectedArray = Array<Any>()
    var titleArray = Array<Any>()
    @IBOutlet weak var viewTabHolder: UIView!
    @IBOutlet weak var viewToBar: UIView!
    @IBOutlet weak var viewForSearchBar: UIView!
    @IBOutlet weak var discoverPropertyHeaderHeightConst: NSLayoutConstraint!
    var propertyMapOrList = 0
    @IBOutlet weak var btnMapOrList: UIButton!
    //Propert List = 0, Property Map = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNeedsStatusBarAppearanceUpdate()
        imgUnSelectedArray = ["explore","heardGray","","bellGray","userGray"]
        imgSelectedArray = ["explore","heardGray","","bellGray","userGray"]
        titleArray = ["Discover","Favourite","","Notification","Profile"]
        container.getTo(0, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewTabHolder.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: -2), radius: 1, scale: true)
        //viewToBar.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
        //viewForSearchBar.layer.borderColor = UIColor.grayColor().cgColor
        //viewForSearchBar.layer.borderWidth = 1
        btnMapOrList.addshadow(top: false, left: true, bottom: true, right: true, shadowRadius: 5.0)
    }
    
    @IBAction func btnPropertyMapOrListingAction(_ sender: Any) {
        if(propertyMapOrList == 4){
            propertyMapOrList = 0
            container.getTo(propertyMapOrList, animated: true)
            btnMapOrList.setImage(UIImage.init(named: "pinRed"), for: .normal)
            btnMapOrList.setTitle("MAP", for: .normal)
        }else{
            propertyMapOrList = 4
            container.getTo(propertyMapOrList, animated: true)
            btnMapOrList.setImage(UIImage.init(named: "pinGray"), for: .normal)
            btnMapOrList.setTitle("LIST", for: .normal)
        }
    }
    
    
    @IBAction func btnFiltersAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnSortByAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SortByPopupVC") as! SortByPopupVC
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        controller.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        self.present(controller, animated: true, completion: nil)
    }

    @IBAction func btnCameraAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LocationConfirmationVC") as! LocationConfirmationVC
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        controller.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        self.present(controller, animated: true, completion: nil)
    }
    
}

extension HomeViewController: DiscoverPropertyListingDelegate, DiscoverPropertyMapViewDelegate{
    func showMapPropertyListing(index: Int) {
        
    }
    func showPropertyList(index: Int) {
        
    }
}

extension HomeViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EMBEDD_TAB"
        {
            container = segue.destination as? PageViewController

            let commonStoryboard=UIStoryboard(name: "Main", bundle: nil)

            let discoverPropertyListVC = commonStoryboard.instantiateViewController(withIdentifier: "DiscoverPropertyListingVC") as! DiscoverPropertyListingVC
            discoverPropertyListVC.mDelegate = self

            let favoriteVC = commonStoryboard.instantiateViewController(withIdentifier: "FavoriteViewController") as! FavoriteViewController

            let notificationVC = commonStoryboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController

            let profileVC = commonStoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            
            let mapVC = commonStoryboard.instantiateViewController(withIdentifier: "DiscoverPropertyMapViewVC") as! DiscoverPropertyMapViewVC
            mapVC.mDelegate = self

            container.arrViewController=[discoverPropertyListVC,favoriteVC,notificationVC,profileVC,mapVC]
        }
    }
}


extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let reuseIdentifier = "Cell"
        let cell = collectionView
        .dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BottomCollectionViewCell
        
        if(indexPath.row != 2){
            cell.imageView.isHidden = false
            cell.lblTitle.isHidden = false
        let imageName: String = self.imgUnSelectedArray[indexPath.row] as! String
        cell.imageView.image = UIImage.init(named: imageName)
        cell.lblTitle.text = self.titleArray[indexPath.row] as? String ?? ""
        cell.lblTitle.textColor = UIColor.grayColor()
        if(slectedCellIndex == indexPath.row){
            let imageName: String = self.imgSelectedArray[indexPath.row] as! String
            cell.imageView.image = UIImage.init(named: imageName)
            cell.lblTitle.textColor = UIColor.redColor()
        }
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var index = 0
        if(indexPath.row == 0){
            //discoverPropertyHeaderHeightConst.constant = 104
            index = propertyMapOrList //indexPath.row
            btnMapOrList.isHidden = false
        }else if(indexPath.row == 1){
            //discoverPropertyHeaderHeightConst.constant = 0
            index = indexPath.row
            btnMapOrList.isHidden = true
        }else if(indexPath.row == 3){
            //discoverPropertyHeaderHeightConst.constant = 0
            index = 2
            btnMapOrList.isHidden = true
        }else if(indexPath.row == 4){
            //discoverPropertyHeaderHeightConst.constant = 0
            index = 3
            btnMapOrList.isHidden = true
        }else{
            return
        }
        
        slectedCellIndex = indexPath.row
        collectionView.reloadData()
        container.getTo(index, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/5)-8, height: 50.0)
    }
}
