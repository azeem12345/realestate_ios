//
//  DiscoverPropertyMapViewVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/29/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SVProgressHUD

protocol DiscoverPropertyMapViewDelegate: class {
    func showPropertyList(index: Int)
}

class DiscoverPropertyMapViewVC: UIViewController {

            @IBOutlet weak var viewTopBar: UIView!
            @IBOutlet weak var viewForSearchBar: UIView!
            weak var mDelegate: DiscoverPropertyMapViewDelegate?
            var latArray: Array<CLLocationDegrees> = [28.630613,28.629141,28.630461,28.629158,28.633489]
            var lngArray: Array<CLLocationDegrees> = [77.378509,77.379145,77.382615,77.380282,77.380572]
            var mapView = GMSMapView()
            var sourceLatAndLng:CLLocationCoordinate2D = CLLocationCoordinate2DMake(28.630613, 77.378509);
            var destLatAndLng:CLLocationCoordinate2D = CLLocationCoordinate2DMake(28.588938, 77.302186);
            @IBOutlet weak var mapViewCustom: UIView!
            //Propert List = 0, Property Map = 4
            fileprivate var locationMarker : GMSMarker? = GMSMarker()
            var customMarker = CustomPinView()
            
            override func viewDidLoad() {
                super.viewDidLoad()
                
                SVProgressHUD.show()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.loadMap()
                    SVProgressHUD.dismiss()
                }
            }
            
            override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(animated)
                viewTopBar.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
                viewForSearchBar.layer.borderColor = UIColor.grayColor().cgColor
                viewForSearchBar.layer.borderWidth = 1

                //print(LocationClass.shared.locationManager.location?.coordinate.latitude)
                
            }
            
            override func viewWillLayoutSubviews() {
                super.viewWillLayoutSubviews()
                
            }
            
            override func viewDidAppear(_ animated: Bool) {
                
            }
            
            func loadMap(){
                
                let camera = GMSCameraPosition.camera(withLatitude: 28.631459, longitude: 77.381863, zoom: 14.0)
                    self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
                
    //            do {
    //              // Set the map style by passing the URL of the local file.
    //              if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
    //                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
    //              } else {
    //                NSLog("Unable to find style.json")
    //              }
    //            } catch {
    //              NSLog("One or more of the map styles failed to load. \(error)")
    //            }
                
                    self.view.addSubview(self.mapView)

                       // Creates a marker in the center of the map.
                       let marker = GMSMarker()
                       marker.position = CLLocationCoordinate2D(latitude: 28.631459, longitude: 77.381863)
                       marker.title = "Wildnet"
                       marker.snippet = "Technologies"
                
                    marker.map = self.mapView
                    self.mapView.delegate = self
                    self.mapView.frame = self.view.frame
                    self.mapViewCustom.addSubview(self.mapView)
                
                
                    for i in 0 ..< self.latArray.count {
                    let state_marker = GMSMarker()
                        state_marker.position = CLLocationCoordinate2D(latitude: self.latArray[i], longitude: self.lngArray[i])
                    state_marker.title = "Title"
                    state_marker.snippet = "Sub Title"
                    state_marker.icon = UIImage.init(named: "currentLocation")
                        state_marker.map = self.mapView
                }
                   
            }
    
    @IBAction func btnFiltersAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FiltersViewController") as! FiltersViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnSortByAction(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SortByPopupVC") as! SortByPopupVC
        controller.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        controller.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        self.present(controller, animated: true, completion: nil)
    }

}


extension DiscoverPropertyMapViewVC: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {

        customMarker.removeFromSuperview()
        customMarker = CustomPinView.instanceFromNib()
        customMarker.layer.cornerRadius = 5.0
        customMarker.clipsToBounds = true
        locationMarker = marker
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
        
        customMarker.center = mapView.projection.point(for: location)
        customMarker.center.y = customMarker.center.y + 30
        
        self.view.addSubview(customMarker)

        return true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
         if (locationMarker != nil){
               guard let location = locationMarker?.position else {
                   print("locationMarker is nil")
                   return
               }
               customMarker.center = mapView.projection.point(for: location)
               customMarker.center.y = customMarker.center.y + 30
           }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        customMarker.removeFromSuperview()
    }
}
