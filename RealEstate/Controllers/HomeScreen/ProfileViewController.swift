//
//  ProfileViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/29/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

enum ProfileEnum: Int {
    case profile = 0, myProperty, myCredits, settings, shareApp
}

class ProfileViewController: UIViewController {
    
    var imgArray = Array<Any>(), titleArray = Array<Any>()
    @IBOutlet weak var headerView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        imgArray = ["","home","coins","coins","share"]
        titleArray = ["","My Properties","My Credits","Settings","Share App"]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerView.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
    }

}


extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = "Cell2"
        if(indexPath.row == 0){
            identifier = "Cell1"
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! ProfileTableViewCell
            if(indexPath.row != 0){
                let imgName = imgArray[indexPath.row] as? String
                    cell.imageView?.image = UIImage.init(named: imgName ?? "")
                let title = titleArray[indexPath.row] as? String
                    cell.lblTitle.text = title
                if(indexPath.row == titleArray.count - 1){
                    cell.separatorView.isHidden = true
                }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectAction = ProfileEnum(rawValue: indexPath.row)
        switch selectAction {
        case .profile:
            break
        case .myProperty:
            break
        case .myCredits:
            break
        case .settings:
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(controller, animated: true)
        case .shareApp:
            break
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 215
        }
        return 50
    }
}
