//
//  FiltersViewController.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/4/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import MultiSlider

class FiltersViewController: BaseClassVC {
    
    @IBOutlet var multiSliderViewForPricing: MultiSlider!
    @IBOutlet var multiSliderViewSquarFeet: MultiSlider!
    @IBOutlet weak var propertyTypeCollectionView: UICollectionView!
    @IBOutlet weak var amenitiesCollectionView: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    var propertyTitleArray = Array<Any>(), propertyImageArray = Array<Any>()
    @IBOutlet var bedroomView: UIView!
    @IBOutlet var bathroomView: UIView!
    @IBOutlet var lblBedroom: UILabel!
    @IBOutlet var lblBathroom: UILabel!
    @IBOutlet var btnBedroomIncrement: UIButton!
    @IBOutlet var btnBathroomIncrement: UIButton!
    @IBOutlet var btnBedroomDecrement: UIButton!
    @IBOutlet var btnBathroomDecrement: UIButton!
    var amenitiesArray = Array<Any>()
    var arraySelectedAmenities = Set<Int>()
    @IBOutlet var btnReset: UIButton!
    @IBOutlet var btnApply: UIButton!
    
    var selectedPropertyType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }
    
    func loadData(){
        propertyTitleArray = ["House","Apt.","Villa"]
        propertyImageArray = ["","",""]
        setUPMultiSlider()
        
        amenitiesArray = ["Balcony","Parking","Pool","Gym","Air-Conditioning","Microvawe","More"]
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(horizontalAlignment: .left,
                                                                        verticalAlignment: .center)
        amenitiesCollectionView.collectionViewLayout = alignedFlowLayout
        
        btnReset.layer.borderWidth = 1
        btnReset.layer.borderColor = UIColor.redColor().cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           headerView.dropShadow(color: .gray, opacity: 0.3, offSet: CGSize(width: -1, height: 2), radius: 1, scale: true)
       }
    
    func setUPMultiSlider(){
        multiSliderViewForPricing.minimumValue = 0
        multiSliderViewForPricing.maximumValue = 10_000_000
        multiSliderViewForPricing.value = [0, 10_000_000]
        multiSliderViewForPricing.valueLabelPosition = .bottom
        multiSliderViewForPricing.keepsDistanceBetweenThumbs = false
        multiSliderViewForPricing.trackWidth = 2
        multiSliderViewForPricing.valueLabels[0].textColor = UIColor.propertySelectedGrayColor()
        multiSliderViewForPricing.valueLabels[1].textColor = UIColor.propertySelectedGrayColor()
        multiSliderViewForPricing.valueLabels[0].font = UIFont.systemFont(ofSize: CGFloat(12.0))
        multiSliderViewForPricing.valueLabels[1].font = UIFont.systemFont(ofSize: CGFloat(12.0))
        
        multiSliderViewForPricing.addTarget(self, action: #selector(sliderChanged(_:)), for: .valueChanged)
        multiSliderViewForPricing.addTarget(self, action: #selector(sliderDragEnded(_:)), for: .touchUpInside)
        
        
        
        multiSliderViewSquarFeet.minimumValue = 0
        multiSliderViewSquarFeet.maximumValue = 2000
        multiSliderViewSquarFeet.value = [0, 2000]
        multiSliderViewSquarFeet.valueLabelPosition = .bottom
        multiSliderViewSquarFeet.keepsDistanceBetweenThumbs = false
        multiSliderViewSquarFeet.trackWidth = 2
        multiSliderViewSquarFeet.valueLabels[0].textColor = UIColor.propertySelectedGrayColor()
        multiSliderViewSquarFeet.valueLabels[1].textColor = UIColor.propertySelectedGrayColor()
        multiSliderViewSquarFeet.valueLabels[0].font = UIFont.systemFont(ofSize: CGFloat(12.0))
        multiSliderViewSquarFeet.valueLabels[1].font = UIFont.systemFont(ofSize: CGFloat(12.0))
               
        multiSliderViewSquarFeet.addTarget(self, action: #selector(sliderChanged(_:)), for: .valueChanged)
        multiSliderViewSquarFeet.addTarget(self, action: #selector(sliderDragEnded(_:)), for: .touchUpInside)
        
    }
    
    @objc func sliderChanged(_ slider: MultiSlider) {
        if(multiSliderViewForPricing == slider){
            let currentValue = Int(slider.value[0])
               multiSliderViewForPricing.value[0] = CGFloat(currentValue)
            let currentValue2 = Int(slider.value[1])
               multiSliderViewForPricing.value[1] = CGFloat(currentValue2)
        }else{
            let currentValue = Int(slider.value[0])
                multiSliderViewSquarFeet.value[0] = CGFloat(currentValue)
            let currentValue2 = Int(slider.value[1])
                multiSliderViewSquarFeet.value[1] = CGFloat(currentValue2)
        }
        
        //print("thumb \(slider.draggedThumbIndex) moved")
        //print("now thumbs are at \(slider.value)") // e.g., [1.0, 4.5, 5.0]
    }

    @objc func sliderDragEnded(_ slider: MultiSlider) {
        
        //print("thumb \(slider.draggedThumbIndex) moved")
        //print("now thumbs are at \(slider.value)") // e.g., [1.0, 4.5, 5.0]
    }
    
    @IBAction func btnIncrementBedroomAndBathroomAction(_ sender: Any) {
        //Bedroom = 1, Bathroom = 2
        let btn = sender as! UIButton
            deSelectBedroomOrBathroom()
        if(btn.tag == 1){
            selectedBedroomOrBathroom(btn1: btnBedroomDecrement, btn2: btnBedroomIncrement, view: bedroomView, label: lblBedroom)
            lblBedroom.text = "\((Int(lblBedroom.text ?? "") ?? 0) + 1)"
        }else{
            selectedBedroomOrBathroom(btn1: btnBathroomIncrement, btn2: btnBathroomDecrement, view: bathroomView, label: lblBathroom)
            lblBathroom.text = "\((Int(lblBathroom.text ?? "") ?? 0) + 1)"
        }
    }
    
    @IBAction func btnDecrementBedroomAndBathroomAction(_ sender: Any) {
        //Bedroom = 1, Bathroom = 2
        let btn = sender as! UIButton
            deSelectBedroomOrBathroom()
        if(btn.tag == 1){
            selectedBedroomOrBathroom(btn1: btnBedroomDecrement, btn2: btnBedroomIncrement, view: bedroomView, label: lblBedroom)
            if((Int(lblBedroom.text ?? "") ?? 0) > 0){
            lblBedroom.text = "\((Int(lblBedroom.text ?? "") ?? 0) - 1)"
            }
        }else{
            selectedBedroomOrBathroom(btn1: btnBathroomIncrement, btn2: btnBathroomDecrement, view: bathroomView, label: lblBathroom)
            if((Int(lblBathroom.text ?? "") ?? 0) > 0){
            lblBathroom.text = "\((Int(lblBathroom.text ?? "") ?? 0) - 1)"
            }
        }
    }
    
    func deSelectBedroomOrBathroom(){
        bedroomView.layer.borderColor = UIColor.white.cgColor
        bathroomView.layer.borderColor = UIColor.white.cgColor
        bedroomView.layer.borderWidth = 1
        bathroomView.layer.borderWidth = 1
        btnBedroomIncrement.titleLabel?.textColor = UIColor.darkGray
        btnBathroomIncrement.titleLabel?.textColor = UIColor.darkGray
        btnBedroomDecrement.titleLabel?.textColor = UIColor.darkGray
        btnBathroomDecrement.titleLabel?.textColor = UIColor.darkGray
    }
    
    func selectedBedroomOrBathroom(btn1: UIButton, btn2: UIButton, view: UIView, label: UILabel){
        btn1.titleLabel?.textColor = UIColor.redColor()
        btn2.titleLabel?.textColor = UIColor.redColor()
        view.layer.borderColor = UIColor.redColor().cgColor
        view.layer.borderWidth = 1
        label.textColor = UIColor.black
    }

}

extension FiltersViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(propertyTypeCollectionView == collectionView){
        return propertyTitleArray.count
        }
        return amenitiesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "Cell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! FilterCollectionVC
        if(propertyTypeCollectionView == collectionView){
            cell.lblTitle.text = propertyTitleArray[indexPath.row] as? String
            cell.lblTitle.textColor = UIColor.propertySelectedGrayColor()
            cell.mainView.backgroundColor = .white
            if(selectedPropertyType == indexPath.row){
                cell.lblTitle.textColor = .white
                cell.mainView.backgroundColor = UIColor.propertySelectedGrayColor()
            }
        }else if(amenitiesCollectionView == collectionView){
            cell.lblTitle.text = amenitiesArray[indexPath.row] as? String
            cell.lblTitle.textColor = UIColor.propertySelectedGrayColor()
            cell.mainView.layer.borderWidth = 1
            cell.mainView.layer.borderColor = UIColor.singleRedColor().cgColor
            cell.mainView.backgroundColor = .white
            if arraySelectedAmenities.contains(indexPath.row){
                cell.lblTitle.textColor = .white
                cell.mainView.backgroundColor = UIColor.singleRedColor()
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(propertyTypeCollectionView == collectionView){
            selectedPropertyType = indexPath.row
            propertyTypeCollectionView.reloadData()
        }else{
            if(self.arraySelectedAmenities.contains(indexPath.row)){
                self.arraySelectedAmenities.remove(indexPath.row)
            }else{
                self.arraySelectedAmenities.insert(indexPath.row)
            }
            self.amenitiesCollectionView.reloadItems(at: [indexPath])
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(propertyTypeCollectionView == collectionView){
            return CGSize(width: 60, height: 60)
        }else{
            let text = amenitiesArray[indexPath.row] as? String
            let label = UILabel(frame: CGRect.zero)
            label.font = UIFont.systemFont(ofSize: CGFloat(12.0))
            label.text = text
            return CGSize(width: label.intrinsicContentSize.width + 34, height: 27)
        }
    }
   
}
