//
//  SortByTableViewCell.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/5/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SortByTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
