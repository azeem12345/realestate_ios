//
//  PropertyListTableViewCell.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/29/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class PropertyListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var carouselView: UIView!
    
    let carousel : ZKCarousel = {
        let carousel = ZKCarousel()
        let slide = ZKCarouselSlide(image: UIImage(named: "property2")!, title: "", description: "")
        let slide1 = ZKCarouselSlide(image: UIImage(named: "property")!, title: "", description: "")
        let slide2 = ZKCarouselSlide(image: UIImage(named: "property")!, title: "", description: "")
        carousel.contentMode = .scaleAspectFit
        carousel.slides = [slide, slide1, slide2]
        return carousel
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //self.carousel.frame = self.carouselView.frame
        //self.carouselView.addSubview(self.carousel)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
