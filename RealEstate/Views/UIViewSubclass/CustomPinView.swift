//
//  CustomPinView.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/5/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class CustomPinView: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblBedroom: UILabel!
    @IBOutlet weak var lblSquarfeet: UILabel!
    @IBOutlet weak var lblBathroom: UILabel!
    
    class func instanceFromNib() -> CustomPinView {
        return UINib(nibName: "CustomPinView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomPinView
       }
}
