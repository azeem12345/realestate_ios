//
//  BottomViewForPhotos.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/28/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class BottomViewForPhotos: UIView {
    
    @IBOutlet weak var exportButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var sparklesButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    class func instanceFromNib() -> BottomViewForPhotos {
        return UINib(nibName: "BottomViewForPhotos", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomViewForPhotos
       }
}
