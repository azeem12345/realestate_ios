//
//  BottomCollectionViewCell.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/30/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class BottomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
