//
//  SavedPhotoCell.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/28/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SavedPhotoCell: UICollectionViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension SavedPhotoCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "Cell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SavedPhotoDetailCell
        let size = (self.collectionView.frame.width/2) - 4
        cell.imageHeightCons.constant = size
        cell.imageWidthCons.constant = size
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (self.collectionView.frame.width/2) - 4
        return CGSize(width: size+1, height: size)
    }

}
