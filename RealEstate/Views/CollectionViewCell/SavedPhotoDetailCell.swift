//
//  SavedPhotoDetailCell.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/28/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class SavedPhotoDetailCell: UICollectionViewCell {
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageWidthCons: NSLayoutConstraint!
    @IBOutlet weak var imageHeightCons: NSLayoutConstraint!
}
