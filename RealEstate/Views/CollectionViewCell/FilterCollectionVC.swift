//
//  FilterCollectionVC.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/4/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit

class FilterCollectionVC: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
