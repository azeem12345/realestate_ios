//
//  PageViewController.h
//  ContainerTabDemo
//
//  Created by Azeem Ahmed on 6/9/17.
//  Copyright © 2017 rnf. All rights reserved.

#import <UIKit/UIKit.h>
@class PageViewController;

@protocol PageViewControllerDelegates <NSObject>

@optional
-(void)didScrollToIndex:(NSInteger)index;

@end

@interface PageViewController : UIPageViewController

@property(weak,nonatomic) id<PageViewControllerDelegates> mDelegate;

@property(assign,nonatomic,getter=isCircular) BOOL circular;
@property(nonatomic,strong) NSArray<UIViewController*> *arrViewController;
@property(assign,nonatomic,readonly) NSInteger currentIndex;



-(void)getToIndex:(NSInteger)index animated:(BOOL)animated;

@end
