//
//  RealEstate
//
//  Created by Azeem Ahmed on 4/16/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import Foundation


struct Constants {
    //Constants Strings messsages
    static let appTitle = "Real Estate"
    static let alerBtnOkText = "Ok"
    static let connectionNotAvailable  = "Please check your internet connection"
    static let NoDataAvailable = "No data available"
    static let pleaseEnterEmail = "Please enter email"
    static let pleaseEnterValidEmail = "Please enter valid email"
    static let pleaseEnterPassword = "Please enter password"
    static let pleaseEnterConfirmPassword = "Please enter confirm password"
    static let pleaseEnterSameConfirmPassword = "Please enter same confirm password"
}


struct Authorization {
    static var token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJuYW1lIjpudWxsLCJpYXQiOjE1ODM3OTAwMjcsImV4cCI6MTU4MzgzMzIyN30.klkxJE84NVpyEI2vTNm91QJL-lpO-Ajd3gjWH3qOxFs"
}


struct Keys{
    static let message = "message"
}


struct AppURL {
    static let development = "https://projects.risians.com/coachers/api/"
}

struct APIKeys {
    static let getCategories = "auth/getCategories"
}
