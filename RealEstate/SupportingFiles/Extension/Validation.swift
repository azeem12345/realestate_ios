//
//  Validation.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/16/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import Foundation

extension String {
    
    var localized:String {
        get {
            return NSLocalizedString(self, comment: self)
        }
    }
    
    static func safelyUnwrap(string:String?) -> String {
        //Returns string if not nil else returns blank
        guard string != nil else {
            return ""
        }
        return string!
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z ]", options: .regularExpression) == nil
    }
    
    var isNumeric: Bool {
        return !isEmpty && range(of: "[^1-9 ]", options: .regularExpression) == nil
    }
    
    static func isStringEmpty(text:String?) -> Bool {
        //Checks if string is not empty
        if let string = text {
            if(string.isEmpty) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return true
        }
    }
    
    static func isEmailValid(email:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: String.safelyUnwrap(string: email))
    }
    
    static func isNameValid(name:String?) -> Bool {
        //Checks if name contains minimum 3 characters
        if(String.safelyUnwrap(string: name).count >= 3) {
            return true
        }
        else {
            return false
        }
    }
    
    static func isMobileNumberValid(mobile:String?) -> Bool {
        if (String.safelyUnwrap(string: mobile).count >= 10 && String.safelyUnwrap(string: mobile).count <= 15) {
            return true
        }
        else {
            return false
        }
    }
    
    static func isMobileNumberContainsAlphabetsOrSpecialCharacters(mobileNumber: String?) -> Bool {
        //Checking if mobile number does not contain alphabets or special characters
        let digitAndSpecialCharacterString = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()\"\"-_=+]{}|;':,./<>?`~/*-+"
        let characterSet = CharacterSet(charactersIn: digitAndSpecialCharacterString)
        if ((mobileNumber as NSString?)?.rangeOfCharacter(from: characterSet))?.location != NSNotFound {
            return true
        } else {
            return false
        }
    }
    
    static func isMobileNumberContainsOnlyZeros(mobileNumber:String?) -> Bool {
        var zeroString:String = ""
        if let count = mobileNumber?.count {
            for _ in 1...count {
                zeroString.append("0")
            }
        }
        if zeroString == mobileNumber {
            return true
        }
        else {
            return false
        }
    }
    
    static func isPasswordValid(password:String?, length: Int) -> Bool {
        //Checks if password contains minimum 8 characters
        if(String.safelyUnwrap(string: password).count >= length) {
            return true
        }
        else {
            return false
        }
    }
}

extension Int {
    static func safelyUnwrap(intValue:Int?) -> Int {
        //Returns string if not nil else returns blank
        guard intValue != nil else {
            return 0
        }
        return intValue!
    }
}

extension Double {
    static func safelyUnwrap(intValue:Double?) -> Double {
        //Returns string if not nil else returns blank
        guard intValue != nil else {
            return 0.0
        }
        return intValue!
    }
}
