//
//  CustomColor.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/16/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
   static func grayColor() -> UIColor {
        return UIColor.init(red: 199.0/255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 1.0)
    }
    
    static func redColor() -> UIColor {
        return UIColor.init(red: 190.0/255.0, green: 12.0/255.0, blue: 2.0/255.0, alpha: 1.0)
    }
    
    static func propertySelectedGrayColor() -> UIColor {
        return UIColor.init(red: 93.0/255.0, green: 192.0/255.0, blue: 158.0/255.0, alpha: 1.0)
    }
    
    static func singleRedColor() -> UIColor {
        return UIColor.init(red: 255.0/255.0, green: 90.0/255.0, blue: 81.0/255.0, alpha: 1.0)
    }
    
    static func darkGrayColor() -> UIColor {
        return UIColor.init(red: 103.0/255.0, green: 104.0/255.0, blue: 104.0/255.0, alpha: 1.0)
    }
    
    static func grayColor115() -> UIColor {
        return UIColor.init(red: 115.0/255.0, green: 115.0/255.0, blue: 115.0/255.0, alpha: 1.0)
    }
    
    static func grayColor95() -> UIColor {
        return UIColor.init(red: 95.0/255.0, green: 95.0/255.0, blue: 95.0/255.0, alpha: 1.0)
    }
    
}
