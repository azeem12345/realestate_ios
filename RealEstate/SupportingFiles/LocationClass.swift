//
//  LocationClass.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 5/1/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationClassDelegate: class {
   func getCurrentLocation()
}


class LocationClass: NSObject {
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    static let shared: LocationClass = LocationClass()
    var permission : ((Bool?)->())?
    weak var mDelegate: LocationClassDelegate?
    var flag: Bool = false
    
    override init() {
         super.init()
        self.setUpLocationManagerDelegate()

        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            guard let currentLocation = locationManager.location else {
                    return
                }
            
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
    }

}

extension LocationClass : CLLocationManagerDelegate {

    fileprivate func setUpLocationManagerDelegate(){
           locationManager = CLLocationManager()
           locationManager.delegate = self
           locationManager.desiredAccuracy = kCLLocationAccuracyBest
           locationManager.requestAlwaysAuthorization()
       }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if let lat  = locations.last?.coordinate.latitude, let long = locations.last?.coordinate.longitude{
            let coordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
            print(coordinates)
            if(!flag){
                mDelegate?.getCurrentLocation()
                flag = true
            }
        }else{
            print("Unable To Access Locaion")
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        switch status {
        case .authorizedAlways,.authorizedWhenInUse:
            print("Good to go and use location")
            locationManager.startUpdatingLocation()
            self.callPermisssionCompletion(val: true)

        case .denied:
            print("DENIED to go and use location")
            self.callPermisssionCompletion(val: false)

        case .restricted:
            print("DENIED to go and use location")
            self.callPermisssionCompletion(val: nil)

        case .notDetermined:
            print("DENIED to go and use location")
            self.callPermisssionCompletion(val: nil)

        default:
            print("Unable to read location :\(status)")
        }
    }
    
    fileprivate func callPermisssionCompletion(val : Bool?){

        guard let comp = self.permission else {
            print("\n\n Unable to  locate completions \n\n")
            return
        }
        if let val =  val{
            comp(val)
        }

    }


}
