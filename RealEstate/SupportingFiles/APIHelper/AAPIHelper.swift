//
//  AAPIHelper.swift
//  DANFO
//
//  Created by Azeem on 12/07/18.
//  Copyright © 2018 Azeem. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class AAPIHelper: NSObject {
    /**
     Make a HTTP GET request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPGET(url:String, parameters:Dictionary<String,Any>?, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) {
        
        if(!Utility.isInternetConnectionAvailable()){
            Utility.showAlertAtTopOfWindowWithMessage(message: Constants.connectionNotAvailable)
            return
        }
        
        // HTTP Get request
        var header: HTTPHeaders = [:]
        //header["Content-Type"] = "application/json"
        header["Authorization"] = Authorization.token
        
        let request = AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON {
            (response: AFDataResponse<Any>) in
            print(response.value ?? "response")
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    AILogManager.printVariable(variable:data)
                    finished(data as! Dictionary<String, Any>)
                }
                break
                
            case .failure(_):
                if let error = response.error {
                    failed(error.localizedDescription)
                    AILogManager.printVariable(variable:error.localizedDescription)
                }
                break
            }
        }
        
        AILogManager.printVariable(variable:request)
    }
    
    func requestHTTPGET2(url:String, parameters:Dictionary<String,Any>?, finished:@escaping (Array<Any>)->Void, failed:@escaping (String)->Void) {
        // HTTP Get request
        var header: HTTPHeaders = [:]
        //header["Content-Type"] = "application/json"
        header["Authorization"] = Authorization.token
        let request = AF.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:AFDataResponse<Any>) in
            print(response.value ?? "response")
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    AILogManager.printVariable(variable:data)
                    if let arrData = data as? Array<Any>{
                    finished(arrData)
                    }else{
                        SVProgressHUD.dismiss()
                        let res = data as! Dictionary<String,Any>
                        Utility.showAlertAtTopOfWindowWithMessage(message:String.safelyUnwrap(string: res[Keys.message] as? String))
                    }
                }
                break
                
            case .failure(_):
                if let error = response.error {
                    failed(error.localizedDescription)
                    AILogManager.printVariable(variable:error.localizedDescription)
                }
                break
            }
        }
        AILogManager.printVariable(variable:request)
    }
    
    
    
    
    /**
     Make a HTTP POST request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPPOST(url:String, parameters:Dictionary<String,Any>, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) {
        // HTTP Get request
        
        var header: HTTPHeaders = [:]
        //header["Content-Type"] = "application/json"
        header["Authorization"] = Authorization.token
        
        AILogManager.printVariable(variable:parameters)
        let request =  AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:AFDataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    AILogManager.printVariable(variable:data)
                    finished(data as! Dictionary<String, Any>)
                }
                break
                
            case .failure(_):
                if let error = response.error {
                    failed(error.localizedDescription)
                    AILogManager.printVariable(variable:error)
                }
                break
            }
        }
        AILogManager.printVariable(variable:request)
    }
    
    /**
     Make a HTTP POST request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPPOSTJSON(url:String, parameters:Dictionary<String,Any>, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) {
        
        if(!Utility.isInternetConnectionAvailable()){
            Utility.showAlertAtTopOfWindowWithMessage(message: Constants.connectionNotAvailable)
            return
        }
        // HTTP Get request
        
        var header: HTTPHeaders = [:]
        //header["Content-Type"] = "application/json"
        header["Authorization"] = Authorization.token
        
        let request = AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response:AFDataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    finished(data as! Dictionary<String, Any>)
                }
                break
                
            case .failure(_):
                if let error = response.error {
                    failed(error.localizedDescription)
                }
                break
            }
        }
        AILogManager.printVariable(variable:request)
    }
    
    func callWebServiceWithMultiPartFormDataAndParams(baseUrl: String, appendingUrl: String, method : HTTPMethod?, parametersDictionary : Dictionary<String,Any>?, mediaDict : Dictionary<String,Any>?, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void)  {
        
        if(!Utility.isInternetConnectionAvailable()){
            Utility.showAlertAtTopOfWindowWithMessage(message: Constants.connectionNotAvailable)
            return
        }

        let url = baseUrl + appendingUrl
        var parameters: Parameters = [:]
        if parametersDictionary != nil {
            for (key, value) in parametersDictionary! {
                parameters[key] = value
            }
        }

        var header: HTTPHeaders = [:]
        header["Authorization"] = Authorization.token
        header["Content-Type"] = "multipart/form-data"
        header["Cache-Control"] = "no-cache"

        //var request : Request? = nil
        
        AF.upload(multipartFormData: {
            multiPartForm in
            
            for (key, value) in parametersDictionary! {
                multiPartForm.append(Data((value as! String).utf8), withName: key)
                //multiPartForm.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            if mediaDict != nil{
                for (key, value) in mediaDict! {
                    multiPartForm.append((value as! UIImage).jpegData(compressionQuality: 1)!, withName: key, fileName: "image.jpeg", mimeType: "image/jpeg")
                }
            }

        }, to: url, method: .post, headers: header)
            .responseJSON { response in
                debugPrint(response)
                
                switch response.result {
                case .success(_):
                    if let data = response.value {
                        finished(data as! Dictionary<String, Any>)
                    }
                    break
                case .failure(let encodingError):
                    failed(encodingError.localizedDescription)
                    break

                }
                
        }

//        AF.upload(multipartFormData: {
//            multiPartForm in
//
//            for (key, value) in parametersDictionary! {
//                multiPartForm.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
//            }
//            if mediaArray != nil{
//                for img in mediaArray!{
//                    multiPartForm.append(UIImageJPEGRepresentation(img as! UIImage, 1)!, withName: "file", fileName: "profilePic.jpeg", mimeType: "image/jpeg")
//                }
//            }
//
//        }, usingThreshold: UInt64.init(), to: url, method: .post, headers:header, encodingCompletion: {
//            encodingResult in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                request = upload
//                upload.responseJSON { response in
//                    print(response)
//                    if let data = response.result.value {
//                        finished(data as! Dictionary<String, Any>)
//                    }
//                }
//            case .failure(let encodingError):
//                print(encodingError)
//                failed(encodingError.localizedDescription)
//                break
//
//            }
//        })
    }
    
    
    func sendGetRequest(_ url: String, parameters: [String: String], completion: @escaping ([String: Any]?, Error?) -> Void) {
        
        if(!Utility.isInternetConnectionAvailable()){
            Utility.showAlertAtTopOfWindowWithMessage(message: Constants.connectionNotAvailable)
            return
        }
        
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,                            // is there data
                let response = response as? HTTPURLResponse,  // is there HTTP response
                (200 ..< 300) ~= response.statusCode,         // is statusCode 2XX
                error == nil else {                           // was there no error, otherwise ...
                    completion(nil, error)
                    return
            }
            
            let responseObject = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
            completion(responseObject, nil)
        }
        task.resume()
    }
    func requestHTTPPUT(url:String, parameters:Dictionary<String,Any>?, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) {
        
        // HTTP Get request
        var header: HTTPHeaders = [:]
        //header["Content-Type"] = "application/json"
        header["Authorization"] = Authorization.token
        let request =    AF.request(url, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:AFDataResponse<Any>) in
            print(response.value ?? "response")
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    AILogManager.printVariable(variable:data)
                    finished(data as! Dictionary<String, Any>)
                }
                break
                
            case .failure(_):
                if let error = response.error {
                    failed(error.localizedDescription)
                    AILogManager.printVariable(variable:error.localizedDescription)
                }
                break
            }
        }
        AILogManager.printVariable(variable:request)
    }
    
    
    func requestHTTPPUTJSON(url:String, parameters:Dictionary<String,Any>, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) {
        // HTTP Get request
        
        var header: HTTPHeaders = [:]
        //header["Content-Type"] = "application/json"
        header["Authorization"] = Authorization.token
        
        let request = AF.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response:AFDataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.value {
                    finished(data as! Dictionary<String, Any>)
                }
                break
                
            case .failure(_):
                if let error = response.error {
                    failed(error.localizedDescription)
                }
                break
            }
        }
        AILogManager.printVariable(variable:request)
    }
    
}
