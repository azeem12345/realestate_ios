//
//  AILogManager.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/16/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//

import UIKit
private var isPrint:Bool = true
class AILogManager: NSObject {
    class var isPrintOn:Bool {
        set(value){
            isPrint = value
        }
        get {
            return isPrint
        }
    }
    
    class func printVariable(variable:Any? ,funcStr: String = #function,line:Int = #line, file:String = #file) {
        if isPrint {
            guard let variable = variable else {
                print("In \(file) with in Function \(funcStr) variable value = nil at line:\(line)")
                return
            }
            print("In \(file) with in Function \(funcStr) variable value = \(variable) at line:\(line)")
        }
    }
    
    
    class func printCurrentTime(line:Int = #line){
        print("current time is \(Date()) at line:\(line)")
    }
}
