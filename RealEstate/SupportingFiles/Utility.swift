//
//  Utility.swift
//  RealEstate
//
//  Created by Azeem Ahmed on 4/16/20.
//  Copyright © 2020 Azeem Ahmed. All rights reserved.
//


import Foundation
import Alamofire


class Utility: NSObject {

static func showAlertAtTopOfWindowWithMessage(message : String){
    
    let alertcontroller : UIAlertController = UIAlertController(title: Constants.appTitle, message: message, preferredStyle: .alert)
    
    let alertActionOkay = UIAlertAction(title: Constants.alerBtnOkText, style: .default, handler: {
        action in
        
    })
    
    alertcontroller.addAction(alertActionOkay)
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    appDelegate.window?.rootViewController?.present(alertcontroller, animated: true, completion: nil)
}
    
    static func showAlertBoxWithoutPresent(message: String){
        let myAlert = UIAlertView()
        myAlert.title = Constants.appTitle
        myAlert.message = message
        myAlert.addButton(withTitle: Constants.alerBtnOkText)
        myAlert.delegate = self
        myAlert.show()
    }
    
   static func formattedDateFromString(dateString: String, withFormat format: String) -> String? {

           let inputFormatter = DateFormatter()
           inputFormatter.dateFormat = "dd/MM/yyyy"

           if let date = inputFormatter.date(from: dateString) {

               let outputFormatter = DateFormatter()
             outputFormatter.dateFormat = format

               return outputFormatter.string(from: date)
           }

           return nil
       }
    
    class func isInternetConnectionAvailable() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    
    static func setUserValue(dict: Dictionary<String,Any>){
        
    }
    
    
}


